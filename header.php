<!--
 =========================================================
 * Material Kit - v2.0.6
 =========================================================

 * Product Page: https://www.creative-tim.com/product/material-kit
 * Copyright 2019 Creative Tim (http://www.creative-tim.com)
   Licensed under MIT (https://github.com/creativetimofficial/material-kit/blob/master/LICENSE.md)


 =========================================================

 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->


<!DOCTYPE html>
<html lang="fr">
 
<head>

    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Plateforme-io</title>
    <link href="image/favicon.ico" rel="icon" type="image/x-icon">
    <meta name="description" content="" />

    <link href="image_portfolio/favicon.ico" rel="icon" type="image/x-icon">
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    
    <!--Fonts and icons-->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!--CSS Files-->
    <link href="assets/css/material-kit.css?v=2.0.6" rel="stylesheet">
    <link href="assets/css/main.css" rel="stylesheet">

</head>
 
<body class="landing-page sidebar-collapse">