<?php include "header.php"; ?>
<?php include "navbar.php"; ?>

<header></header>
  <div class="page-header header-filter clear-filter black-filter" data-parallax="true">
    <div class="container">
      <div class="row">
        <div class="col-md-8 ml-auto mr-auto">
          <div class="brand text-center">
            <div class="header_mobile"></div>                               
          </div>
        </div>
      </div>
    </div>
  </div>  
  <div class="main main-raised ">
    <div class="container background_body">
      <div class="section text-center">
        <div class="row">
          <div class="col-md-10 ml-auto mr-auto">  
          <div class="space-70"></div>
            <div class="jumbotron">
             <!-- formulaire temporairea effet visuel uniquement, a changer avec des concordences php -->
              <div id="second-block">
                <form method="post" id="contact-form">
                  <div class="form-header">
                    <h1>Contact form</h1>
                    <h3>Envie d'un cours ou besoin de plus d'informations sur un sujet en particulier ?</h3> 
                    <h4>Envoyez nous un message via ce formulaire ou par les moyens mis a disposition dès maintenant !</h4> 
                  </div>
                  <div class="form-block">
                    <label>Nom complet</label>
                      <input type="text" name="name" placeholder="Votre Nom" value="" required/>
                  </div>
                  <div class="form-block">
                    <label>Adresse mail</label>
                      <input type="tel" name="mail" placeholder="Votre adresse Mail" value=""/>
                  </div>
                  <div class="form-block">
                    <label>Object</label>
                      <input type="text" name="object" placeholder="Object du message" value=""/>
                  </div>
                  <div class="space-30"></div>
                  <div class="form-block">
                    <label>Message</label>
                      <textarea name="message" placeholder="Votre message"></textarea>
                  </div>
                  <div class="space-30"></div>
                  <a type="button" class="btn btn-outline-primary">Envoyer</a>                           
                </form>
              </div>
            </div>
            <div class="space-30"></div>
            <p>Contactez nous par : <br> Slack, Discord ou Gmail</p>
            <div class="container">
              <div class="space-30"></div>
              <a rel="tooltip" data-placement="bottom" data-original-title="tourat.kevin@gmail.com" href="mailto:tourat.kevin@gmail.com"><img class="svg-two" src="image/profile-max.png" alt="kevin"></a>
              <a rel="tooltip" data-placement="bottom" data-original-title="tourat.kevin@gmail.com" href="mailto:tourat.kevin@gmail.com"><img class="space-footer svg-two" src="image/profile_kevin-black.png" alt="kevin"></a>
              <div class="space-30"></div>
              <a href=""><img class="svg" src="image/slack.png" alt="slack"></a>
              <a href=""><img class="space-footer svg" src="image/discord.png" alt="discord"></a>                   
              <div class="space-30"></div>
            </div>                                 
          </div>
        </div>
      </div>
    </div>
  </div>     
</div>

<?php include "footer.php"; ?>